<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>event</title>


	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
	<div class="container">
		<a href="#" class="btn btn-success btn-add">Add</a>
		<div class="table-responsive">
			<table class="table table-hover table-striped" id="data_event">
				<thead class="thead-dark">
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Update at</th>
						<th class="no-sort">Action</th>
					</tr>
				</thead>
				<tbody>
					@php
					$index =1; 
					@endphp
					@foreach ($events as $event)
					<tr id="row_id_{{$event->id}}">
						<td>{{$index++}}</td>
						<td>{{$event->title}}</td>
						<td>{{$event->updated_at}}</td>
						<td> 
							<button type="button" class="btn btn-success btn-detail" data-url="{{route('events-ajax.show',$event->id)}}">Detail</button>
							<button type="button" class="btn btn-warning btn-edit" data-url="{{route('events-ajax.edit',$event->id)}}">Edit</button>
							<button type="button" class="btn btn-danger btn-delete"  data-url="{{route('events-ajax.destroy',$event->id)}}">Delete</button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</body>
{{--modal detail--}}
<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="name_event" style="text-transform: uppercase;"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label class="form-control-label">Detail:</label>
						<h3 id="content_event" class="form-control"></h3>
					</div>
					<div class="form-group">
						<label class="form-control-label">Time:</label>
						<h3 id="time_event" class="form-control"></h3>
					</div>
					<div class="form-group">
						<label class="form-control-label">Location:</label>
						<h3 id="location_event" class="form-control"></h3>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

{{--modal add--}}
<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" style="text-transform: uppercase;"> New Event</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="" data-url="{{route('events-ajax.store')}}" id="form_add"  method="POST" role="form">
				<div class="modal-body">
					<div class="form-group">
						<label class="form-control-label" for="name_add">Title</label>
						<input type="text" class="form-control" name="name_add" id="name_add" placeholder="title">
						<span id="error_name_add" class="text-danger"></span>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="content_add">Content</label>
						<input type="text" class="form-control" name="content_add" id="content_add" placeholder="content">
						<span id="error_content_add" class="text-danger"></span>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="location_add">Location</label>
						<input type="text" class="form-control" name="location_add" id="location_add" placeholder="Location">
						<span id="error_location_add" class="text-danger"></span>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="time_add">Time:</label>
						<input name="time_add" type="datetime-local" class="form-control" id="time_add" value="2019-01-01T00:00:01" >
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>

{{--modal edit--}}
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" style="text-transform: uppercase;"> Update Event</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form  id="form_edit"  method="POST" role="form">
				{{ method_field('PUT') }}
				<div class="modal-body">
					<div class="form-group">
						<label class="form-control-label" for="name_edit">Title</label>
						<input type="text" class="form-control" name="name_edit" id="name_edit" placeholder="title">
						<span id="error_name_edit" class="text-danger"></span>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="content_edit">Content</label>
						<input type="text" class="form-control" name="content_edit" id="content_edit" placeholder="content">
						<span id="error_content_edit" class="text-danger"></span>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="location_edit">Location</label>
						<input type="text" class="form-control" name="location_edit" id="location_edit" placeholder="location">
						<span id="error_location_edit" class="text-danger"></span>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="time_edit">Time:</label>
						<input name="time_edit" type="datetime-local" class="form-control" id="time_edit" value="2019-01-01T00:00:01" >
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" ></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet">

<script type="text/javascript" charset="utf-8">
	window.Laravel = <?php echo json_encode([
		'csrfToken' => csrf_token(),
		]); ?>

	$(document).ready(function () {
		$('#data_event').dataTable( {
			"columnDefs": [ {
				"targets"  : 'no-sort',
				"orderable": false,
				"order": []
			}]
		});

		$('#data_event').on('click', '.btn-detail', function() {
			$('#modal_detail').modal('show');

			var url = $(this).attr('data-url');

			$.ajax({
				url: url,
				type: 'get',
				success: function(response){
					$('#name_event').text(response.data.title);
					$('#content_event').text(response.data.content);
					$('#time_event').text(response.data.time);
					$('#location_event').text(response.data.location);
				}
			});
		});
		
		$('#data_event').on('click', '.btn-delete', function() {
			var url = $(this).attr('data-url');

			if (confirm('Are you sure?')) {
				$.ajax({
					url: url,
					type: 'delete',
					data: {"_token": "{{ csrf_token() }}"},
					success: function(res)
					{
						toastr.warning('delete successful!','Notification',{timeOut: 2000});				
						$('#row_id_'+res.data).remove();

					}
				});
			}
		});

		$('.btn-add').click(function(e){
			e.preventDefault();
			$('#modal_add').modal('show');
		});

		$('#form_add').submit(function(e){
			e.preventDefault();
			var error_name = '';
			var error_content = '';
			var error_location = '';

			if($('#name_add').val()==''){
				error_name = 'title is required';
				$('#error_name_add').text(error_name);
				$('#name_add').css('border-color','#cc0000');
			}else
			{
				error_name = '';
				$('#error_name_add').text(error_name);
				$('#name_add').css('border-color','');
			}

			if ($('#content_add').val()=='') {
				error_content = 'content is required';
				$('#error_content_add').text(error_content);
				$('#content_add').css('border-color','#cc0000');
			} else {
				error_content = '';
				$('#error_content_add').text(error_content);
				$('#content_add').css('border-color','');
			}

			if ($('#location_add').val()=='') {
				error_location = 'locaion is required';
				$('#error_location_add').text(error_location);
				$('#location_add').css('border-color','#cc0000');
			} else {
				error_location = '';
				$('#error_location_add').text(error_location);
				$('#location_add').css('border-color','');
			}

			if (error_content!='' || error_name!= ''|| error_location!= '') {
				return false;
			} else {
				var url = $(this).attr('data-url');
				$.ajax({
					type: 'post',
					url: url,
					data: {
						"_token": "{{ csrf_token() }}",
						"title": $('#name_add').val(),
						"content": $('#content_add').val(),
						"location": $('#location_add').val(),
						"time": $('#time_add').val()
					},
					dataType:'JSON',
					success: function (response) {

						$('#form_add').trigger("reset");
						$('#modal_add').modal('hide');
						$('#data_event').prepend(
							`<tr id="row_id_`+response.data.id+`">
							<td>`+{{$index++}}+`</td>"
							<td>`+response.data.title+`</td>"
							<td>`+response.data.updated_at+`</td>"
							<td>
								<button type="button" class="btn btn-success btn-detail" data-url="/events-ajax/`+response.data.id+`">Detail</button>
								<button type="button" class="btn btn-warning btn-edit" data-url="/events-ajax/`+response.data.id+`/edit">Edit</button>
								<button type="button" class="btn btn-danger btn-delete" data-url="/events-ajax/`+response.data.id+`">Delete</button>
							</td>
						</tr>`
						);
						toastr.success('Add successful!','Congratulations',{timeOut: 2000});
					},
					error: function (jqXHR, textStatus, errorThrown) {
					}
				});
			}
		});

		$('#data_event').on('click', '.btn-edit', function() {

			$('#modal_edit').modal('show');
			var url = $(this).attr('data-url');

			$.ajax({
				url: url,
				type: 'get',
				success: function(res){
					$('#name_edit').val(res.data.title);
					$('#content_edit').val(res.data.content);
					$('#location_edit').val(res.data.location);
					var time = res.data.time.replace(" ", "T");
					$('#time_edit').val(time);
					$('#form_edit').attr('data-url','{{ asset('events-ajax/') }}/'+res.data.id);
				}
			});
			
			$('#form_edit').submit(function(e){
				e.preventDefault();
				var error_name = '';
				var error_content = '';
				var error_location = '';

				if($('#name_edit').val()==''){
					error_name = 'title is required';
					$('#error_name_edit').text(error_name);
					$('#name_edit').css('border-color','#cc0000');
				}else
				{
					error_name = '';
					$('#error_name_edit').text(error_name);
					$('#name_edit').css('border-color','');
				}

				if ($('#content_edit').val()=='') {
					error_content = 'content is required';
					$('#error_content_edit').text(error_content);
					$('#content_edit').css('border-color','#cc0000');
				} else {
					error_content = '';
					$('#error_content_edit').text(error_content);
					$('#content_edit').css('border-color','');
				}

				if ($('#location_edit').val()=='') {
					error_location = 'locaion is required';
					$('#error_location_edit').text(error_location);
					$('#location_edit').css('border-color','#cc0000');
				} else {
					error_location = '';
					$('#error_location_edit').text(error_location);
					$('#location_edit').css('border-color','');
				}

				if (error_content!='' || error_name!= ''|| error_location!= '') {
					return false;
				} else {
					var url = $(this).attr('data-url');
					$.ajax({
						type: 'put',
						url: url,
						data: {
							"_token": "{{ csrf_token() }}",
							"title": $('#name_edit').val(),
							"content": $('#content_edit').val(),
							"location": $('#location_edit').val(),
							"time": $('#time_edit').val()
						},
						dataType:'JSON',
						success: function (response) {
							var col =$('#row_id_'+response.data.id).children("td");
							$(col[1]).text(response.data.title);
							$(col[2]).text(response.data.updated_at);
							$('#form_edit').trigger("reset");
							$('#modal_edit').modal('hide');
							toastr.success('update successful!','Congratulations',{timeOut: 2000});
						},
						error: function (jqXHR, textStatus, errorThrown) {
							console.log('aaaaaaa');
						}
					});
				}
			});

		});
	})
</script>
</html>
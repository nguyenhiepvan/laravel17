<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Add event</title>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>

</head>
<body>
	<div class="container">
		<form action="{{ route('events.store') }}" method="POST" class="" role="form">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group">
				<legend>Add event</legend>
			</div>
			<div class="form-group">
				<label class="control-label" for="title">Title:</label>
				<input name="title" type="text" class="form-control" id="title" placeholder="Enter title">
			</div>
			<div class="form-group">
				<label class="control-label" for="content">Content:</label>
				<input name="content" type="text" class="form-control" id="content" placeholder="Enter content">
			</div>
			<div class="form-group">
				<label class="control-label" for="time">time:</label>
				<input name="time" type="datetime-local" class="form-control" id="time" value="2014-11-16T15:25:33" >
			</div>
			<div class="form-group">
				<label class="control-label" for="location">Location:</label>
				<input name="location" type="text" class="form-control" id="location" placeholder="Enter location">
			</div>
			<div class="form-group">
				<div class="">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
class EventController extends Controller
{
    //
	public function index()
	{
   $events=Event::all();
		//dd($events);
   return view('event',compact('events')); 

 } 
 public function create()
 {
  return view('add'); 
}

public function store(Request $request)
{
  Event::create($request->all());
  return redirect(route('events.index'));
}

/**
     * phương thức sửa
     * @param  integer $id id của event cần sửa
     * @return view là form sửa với dữ liệu hiện tại của event
     */
public function edit($id)
{
  $event=Event::find($id);

  return view('edit',compact('event','id'));
}


public function update(Request $request,$id)
{
  Event::find($id)->update($request->all());
  return redirect(route('events.index'));
}

public function destroy($id)
{
  Event::find($id)->delete();
  return redirect(route('events.index'));
}

}
